#!/bin/bash
# Rewrite randomly the date of every commit of a branch, but preserve current time of the day
# Arguments are:
#  - a number betwwen 1 and 12 as start month
#  - a number betwwen 1 and 12 as end month
#  - [optionnal] a year

# Example:
# Rewrite current branch commits to all have a date between june and august 2022
#   $sh ./git_hist.sh 6 8 2022

# Todo: cross year

# Dates and user input
startMonth=$1
endMonth=$2
userYear=$3
currentYear=$(date +%Y)
year=${userYear:-$currentYear}

interv=$((endMonth-startMonth))

startDate=$(date -d "$year-$startMonth-01" +%s)
echo "From: $(date -d @$startDate)"
endDate=$(date -d "$year-$endMonth-01" +%s)
echo "To: $(date -d @$endDate)"

# Current Git branch
branchName=$(git branch --show-current)
commits=$(git rev-list $branchName ^master)
commitsCount=$(git rev-list --count $branchName ^master)
echo "\nNumber of commits: $commitsCount"

echo "\n"
# Random dates
counter=0

shuf -n$commitsCount -i$(date -d "$year-$startMonth-01" +%s)-$(date -d "$year-$endMonth-01" +%s)\
| sort -d \
| while read newRawDate; do
 	newDate=$(date -d @$newRawDate)

 	longSHA=$(git log --oneline --no-abbrev | head -n $((counter+1)) | tail -n 1 | awk '{print $1}')
 	counter=$((counter+1))

 	oldTime=$(git show --no-patch --format=%ci $longSHA| awk '{print($2)}')

 	newDateOldTime=$(echo $newDate | sed "s/..:..:.. .M/$oldTime/")

 	# TODO: remove main dep and make it work with detached HEAD
 	git filter-branch -q -f --env-filter \
		'if [ $GIT_COMMIT = "'"${longSHA}"'" ]
		then
			export GIT_AUTHOR_DATE="'"${newDateOldTime}"'"
			export GIT_COMMITTER_DATE="'"${newDateOldTime}"'"
		fi' main...$branchName
done
